# Econ-ResearchMethods Introduction
This repository contains course materials for the graduate-level Research Methods for Economics and Business. 

## Introduction
This course is intended to train students in frontier research methods for good empirical and quantitative studies in areas related to economics and business. This course covers a variety of research tools and approaches and their applications in economics, business, finance, and management. It emphasizes a collaboration of various quantitative skills in conducting economic research, as well as a tight connection between the theory and empirical facts. 

The course begins with introducing various databases and web-scrapping methods of collecting online data. It then introduces data visualization, the classical econometric frameworks, and recent developments in machine learning and text analysis. Then it discusses the establishment of economic theories behind the observed empirical facts, including both verbal-based and math-based models. The course concludes by introducing basic computational methods in estimating and simulating economic models to perform the counterfactual analysis.

## Contents

* __Part 1__: Working with data effectively: Using `STATA` and `Python` in `Jupyterlab`; Various sources of Chinese Data
* __Part 2__: Choosing the right econometric tools: Panel data models, Diff-in-Diff, simulations, machine learning methods, text analysis
* __Part 3__: Seeking the theory behind the empirical facts
* __Part 4__: Model computation and simulation 
